// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();



}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
    FVector RandLocation(FMath::RandRange(-880.f, 880.f), FMath::RandRange(-880.f, 880.f), 0);
    FTransform Location(RandLocation);
    if (bIsHead)
    {
        int RandomNumber = FMath::RandRange(0, 10);
        auto Snake = Cast<ASnakeBase>(Interactor);
        if (IsValid(Snake))
        {
           Snake->Hunger = 1;
           Snake->GetScore();
           Snake->AddSnakeElement(1);
           GetWorld()->SpawnActor<AFood>(this->GetClass(), Location);
           if (RandomNumber <= 2)
           {
               Snake->SetMovementSpeedFast();
           }
           if (RandomNumber > 2 && RandomNumber <= 4)
           {
               Snake->SetMovementSpeedSlow();
           }
           if (RandomNumber > 4 && RandomNumber <= 6)
           {
               Snake->GetScore();
           }
           Destroy();
        }
    }
}






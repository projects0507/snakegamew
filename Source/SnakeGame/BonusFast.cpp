// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusFast.h"
#include "SnakeBase.h"

// Sets default values
ABonusFast::ABonusFast()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABonusFast::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABonusFast::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABonusFast::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SetMovementSpeedFast();
			Destroy();
		}
	}
}


// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"



// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementSize = 100.f;
	MovementSpeed = 1.f;
	LastMoveDirection = EMovementDirection::DOWN;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElement(3);
	TickSpeed = 0.5;
	Score = 0;
	Hunger = 1;
	bIsDied = false;
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	SetActorTickInterval(TickSpeed);
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		const FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		const FTransform NewTransform(NewLocation);
		auto NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SetActorHiddenInGame(true);
		NewSnakeElement->SnakeOwner=this;
		const int32 ElemIndex = SnakeElements.Add(NewSnakeElement);
		if (ElemIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{

	FVector MovementVector(ForceInitToZero);
	
	switch (LastMoveDirection)
	{
			case EMovementDirection::UP:
				MovementVector.X += ElementSize;
				break;
			case EMovementDirection::DOWN:
				MovementVector.X -= ElementSize;
				break;
			case EMovementDirection::LEFT:
				MovementVector.Y += ElementSize;
				break;
			case EMovementDirection::RIGHT:
				MovementVector.Y -= ElementSize;
				break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
	
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
		SnakeElements[i]->SetActorHiddenInGame(false);
	}
	SnakeElements[0]->SetActorHiddenInGame(false);
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}


void ASnakeBase::SetMovementSpeedSlow()
{
	TickSpeed = TickSpeed * 1.2;
}

void ASnakeBase::SetMovementSpeedFast()
{
	TickSpeed = TickSpeed * 0.8;
}

void ASnakeBase::TryChangeDirection(EMovementDirection& Current)
{
	bool MovementValid;
	switch (Current)
	{
	case EMovementDirection::UP:
		MovementValid = TempMovementDirection != EMovementDirection::DOWN;
		break;
	case EMovementDirection::DOWN:
		MovementValid = TempMovementDirection != EMovementDirection::UP;
		break;
	case EMovementDirection::RIGHT:
		MovementValid = TempMovementDirection != EMovementDirection::LEFT;
		break;
	case EMovementDirection::LEFT:
		MovementValid = TempMovementDirection != EMovementDirection::RIGHT;
		break;
	default:
		MovementValid = false;
	}
	if(MovementValid)
	{
		Current = TempMovementDirection;
	}

}

void ASnakeBase::GetScore()
{
	Score = Score + 10;
}

void ASnakeBase::SetHunger()
{
	Hunger = 1;
}

void ASnakeBase::DeathWallOverlap()
{
}

void ASnakeBase::SetbIsDied()
{
	bIsDied = 1;
}














 
